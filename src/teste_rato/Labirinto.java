package teste_rato;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Labirinto {
	
	public static void main(String[] args) throws Exception {
		Labirinto labirinto = new Labirinto();
		Integer[][] rooms = null;
		try(Stream<String> content = Files.lines(Paths.get("labirinto.txt"), StandardCharsets.UTF_8)){
			rooms = content.map(line -> Stream.of(line.split("")).map(e -> (e.equals("x") ? -1 : 0)).toArray(size -> new Integer[size])).toArray(size -> new Integer[size][1]);
			// se a entrada ou a saida estiverem bloqueadas, nao em solucao
			if(rooms[0][0] == -1 || rooms[rooms.length-1][rooms.length-1] == -1) {
				throw new IOException("sem solucao");
			}
			
			labirinto.move(rooms, rooms.length-1, rooms.length-1, 0);
		} catch (IOException e) {
			labirinto.print(rooms);
		}
	}
	
	private void print(Integer[][] labs) {
		System.out.print("  ");
		for(int i = 0; i < labs.length; i ++) System.out.print(String.format("%2d ", i));
		System.out.println();
		System.out.print("   ");
		for(int i = 0; i < labs.length; i ++) {
			System.out.print("---");
		}
		System.out.println("  ");
		for(int i = 0; i < labs.length; i ++) {
			System.out.print(i + " |");
			for(int j = 0; j < labs[i].length; j ++) {
				System.out.print((labs[i][j] == -1 ? "xx" : (labs[i][j] == 0 ? "  " : String.format("%2d", labs[i][j]))) + " ");
			}
			System.out.println("|");
		}
		System.out.print("   ");
		for(int i = 0; i < labs.length; i ++) System.out.print("---");
		System.out.println("  ");
	}

	private void move(Integer[][] labs, int x, int y, int index) throws IOException {
		int max = labs.length-1;
		if(x > max || x < 0 )     return; // se achei as paredes dir ou esq
		if(y > max || y < 0 )     return; // se achei as paredes sup ou inf
		if(labs[x][y] == -1)     return; // se esta bloqueado, volte
		if(labs[x][y] > 0) return; // se ja visitei, volte
		index = lookArround(labs, x, y, index) + 1;
		labs[x][y] = index; 
		if (x == 0 && y == 0) throw new IOException("Cheguei");
		print(labs);
		move(labs, x, y - 1, index); // move para a esquerda
		move(labs, x + 1, y, index); // move para baixo
		move(labs, x, y + 1, index); // move para a direita
		move(labs, x - 1, y, index); // move para cima
	}
	
	private int lookArround(Integer[][] labs, int x, int y, int index) throws IOException {
		
		int left = index;
		int right = index;
		int up = index;
		int down = index;
		
		if(x + 1 <= labs.length-1) down = labs[x+1][y] > 0 ? labs[x+1][y] : index;
		if(y + 1 <= labs.length-1) right = labs[x][y+1] > 0 ? labs[x][y+1] : index;
		if(x - 1 >= 0) up = labs[x-1][y] > 0 ? labs[x-1][y] : index;
		if(y - 1 >= 0) left = labs[x][y-1] > 0 ? labs[x][y-1] : index;
		
		int[] mins = {index, left, right, up, down};
		int min = IntStream.of(mins).min().getAsInt();
		return min;
	}
}
